import React from 'react'
import './style.css';
function Form() {
    return (
        <div className='main-div'>

            <div className='row'>
            <div class="col-md-12">
            <div class="container" style={{
                    marginLeft: "10px",
                    color: "#000000b8"
            }}>
                <h4>New Person</h4>
                </div>
            </div>
                <div class="col-md-6">
                    <div class="container">
                        <div class="row lt-email">
                            <div class="col-md-6">
                                <div class="row ">
                                    <label for="inputEmail" class="col-sm-4 col-form-label">HMO Code</label>
                                    <div class="col-sm-8"><input type="email" class="form-control" id="inputEmail" placeholder="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"><div class="row ">
                                <div class="col-sm-10 offset-sm-2"><div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="checkRemember" />
                                    <label class="form-check-label" for="checkRemember">Anonymous Person</label>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="row lt-email"><div class="col-md-6">
                            <div class="row ">
                                <label for="inputEmail" class="col-sm-4 col-form-label">Person SSN</label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control" id="inputEmail" placeholder="" />
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row lt-email">
                            <div class="col-md-12">
                                <div class="row ">
                                    <label for="inputEmail" class="col-sm-3 col-form-label">Person Mem ID</label>
                                    <div class="col-sm-6 p-0">
                                        <input type="email" class="form-control" id="inputEmail" placeholder="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row lt-email">
                            <div class="col-md-12">
                                <div class="row ">
                                    <label for="inputEmail" class="col-sm-4 col-form-label">Disease Management Status </label>
                                    <div class="col-sm-5 p-0">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Select Status</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <fieldset class="border rounded-3 p-3 mb-3">
                            <legend class="float-none w-auto">Name</legend>
                            <div class="row ">
                                <label for="inputEmail" class="col-sm-2 col-form-label">Last</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail" placeholder="" required />
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">First</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword" placeholder="" required />
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">M.I</label>
                                <div class="col-sm-10">
                                    <input class="form-check-input" type="checkbox" id="checkRemember" />
                                </div>
                            </div>

                        </fieldset>
                    </div>

                    <div class="container">
                        <fieldset class="border rounded-3 p-3 mb-3">
                            <legend class="float-none w-auto">General Information</legend>
                            <div class="row ">
                                <label for="inputEmail" class="col-sm-2 col-form-label">Gender</label>
                                <div class="col-sm-10">
                                    <select class="form-select" aria-label="Default select example">
                                        <option selected>Select Gender</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">DOB</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="inputPassword" placeholder="" />
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Age</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputPassword" placeholder="" />
                                </div>
                            </div>

                        </fieldset>
                    </div>


                    <div class="container">
                        <fieldset class="border rounded-3 p-3 mb-3">
                            <legend class="float-none w-auto">Insured</legend>
                            <div class="row ">
                                <label for="inputEmail" class="col-sm-2 col-form-label">Relation to person</label>
                                <div class="col-sm-6">
                                    <select class="form-select" aria-label="Default select example">
                                        <option selected>Select</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-primary">Insured Find</button>
                                </div>
                            </div>


                        </fieldset>
                    </div>
                </div>
                <div className='col-md-6'>
                    <div class="container">
                        <fieldset class="border rounded-3 p-3">
                            <legend class="float-none w-auto">Address</legend>
                            <div class="row ">
                                <label for="inputEmail" class="col-sm-2 col-form-label">Address 1</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail" placeholder="" required />
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Address 2</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword" placeholder="" required />
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Zip Code</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword" placeholder="" required />
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">City</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword" placeholder="" required />
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">County</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword" placeholder="" required />
                                </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">State</label>
                                <div class="col-sm-10 p-0">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Select State</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="row ">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Country</label>
                                <div class="col-sm-10 p-0">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Select Country</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                            </div>
                        </fieldset>
                    </div>

                    <div class="container">
                        <fieldset class="border rounded-3 p-3">
                            <legend class="float-none w-auto">Phones</legend>
                            <div className='row'>
                                <div className='col-md-8' style={{ height: "40vh" ,overflowX:"scroll",border:"2px solid #8080806b"}}>
                                    <table class="table table-bordered" style={{width:"110%",marginLeft:"-13px"}}>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Phone Type</th>
                                                <th>Phone</th>
                                                <th>Extention</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {/* <tr>
                                                <td>1</td>
                                                <td>Clark</td>
                                                <td>Kent</td>
                                                <td>clarkkent@mail.com</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Peter</td>
                                                <td>Parker</td>
                                                <td>peterparker@mail.com</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>John</td>
                                                <td>Carter</td>
                                                <td>johncarter@mail.com</td>
                                            </tr> */}
                                        </tbody>
                                    </table>
                                </div>
                                <div className='col-md-4'>
                                    <button type="button" class="btn btn-outline-primary" style={{ width: "100%", margin: "0px 0px 15px 10px" }}>Add</button><br />
                                    <button type="button" class="btn btn-outline-primary" style={{ width: "100%", margin: "0px 0px 15px 10px" }}>Modify</button><br />
                                    <button type="button" class="btn btn-outline-primary" style={{ width: "100%", margin: "0px 0px 15px 10px" }}>Delete</button><br />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="footer-div mb-3">
                <div class="row">
                    <div class="btns">
                        <button type="button" class="btn btn-primary">Primary</button>
                        <button type="button" class="btn btn-secondary">Secondary</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Form