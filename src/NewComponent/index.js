import React from 'react'
import OutlinedDiv from './outlinedDiv'
import { Grid, TextField, Typography, Container, Box, Button } from '@mui/material';
import { makeStyles } from '@mui/styles';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CustomizedTables from './table';






const useStyles = makeStyles({
  root: {
    minWidth: 275,
    border: "1px solid",
    padding: "10px",
    boxShadow: "5px 10px red"
  },
//other styles and classes//
})

function NewPerson() {
  const classes= useStyles()
  const [data, setData] = React.useState('');

  const handleChange = (event) => {
    setData(event.target.value);
  };
  return (
    <>
     <div class="card card-5">
      <Grid container spacing={2}>
        <Grid item lg={6} md={6} xs={12} sm={12}>

          <Box
            px={3} py={2}
            container
            direction="row"
          >
            <Grid container spacing={1}>
              <Grid item xs={12} sm={6}>
                <TextField
                  id="outlined-textarea"
                  label="HMO Code"
                  placeholder="HMO Code"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Checkbox defaultChecked /> Anonymous Person
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <TextField
                  required
                  name="fullname"
                  label="Name"
                  fullWidth
                  margin="dense"
                  id="outlined-required"
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <TextField
                  required
                  name="fullname"
                  label="Person Mem Id"
                  fullWidth
                  id="outlined-required"
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Status</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={data}
                    label="Status"
                    onChange={handleChange}
                  >
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>


          </Box>

          <Box
            px={3} py={2}
            container
            direction="row"
          >
            <OutlinedDiv label="Name">
              <Grid container spacing={1}>
                <Grid item xs={12} sm={12}>
                  <TextField
                    id="outlined-textarea"
                    label="Last"
                    fullWidth
                    placeholder="HMO Code"
                  />
                </Grid>
                <Grid item xs={12} sm={12}>
                  <TextField
                    id="outlined-textarea"
                    label="First"
                    fullWidth
                    placeholder="HMO Code"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextField
                    name="fullname"
                    label="M.I."
                    fullWidth
                    margin="dense"
                    id="outlined-required"
                  />
                </Grid>
              </Grid>
            </OutlinedDiv>
          </Box>

          <Box
            px={3} py={2}
            container
            direction="row"
          >
            <OutlinedDiv label="General Information">
              <Grid container spacing={1}>
                <Grid item xs={12} sm={12}>
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Gender</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={data}
                      label="Gender"
                      onChange={handleChange}
                    >
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DemoContainer components={['DatePicker']}>
                      <DatePicker label="Basic date picker" />
                    </DemoContainer>
                  </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextField
                    name="fullname"
                    label="Age"
                    fullWidth
                    margin="dense"
                    id="outlined-required"
                  />
                </Grid>
              </Grid>
            </OutlinedDiv>
          </Box>

          <Box
            px={3} py={2}
            container
            direction="row"
          >
            <OutlinedDiv label="Insured">
              <Grid container spacing={1}>

                <Grid item xs={12} sm={6} lg={6}>
                  <TextField
                    name="fullname"
                    label="Person"
                    fullWidth
                    margin="dense"
                    id="outlined-required"
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6} sx={{ marginTop: "2%" }}>
                  <Button variant="contained">Insured Find</Button>

                </Grid>
              </Grid>
            </OutlinedDiv>
          </Box>
        </Grid>
        <Grid item lg={6} md={6} xs={12} sm={12}>
          <Box
            px={3} py={2}
            container
            direction="row"
          >
            <OutlinedDiv label="Address">
              <Grid container spacing={1}>

                <Grid item xs={12} sm={12} lg={12}>
                  <TextField
                    name="fullname"
                    label="Addres 1"
                    fullWidth
                    margin="dense"
                    id="outlined-required"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextField
                    name="fullname"
                    label="Address 2"
                    fullWidth
                    margin="dense"
                    id="outlined-required"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextField
                    name="fullname"
                    label="Zip Code"
                    fullWidth
                    margin="dense"
                    id="outlined-required"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextField
                    name="fullname"
                    label="City"
                    fullWidth
                    margin="dense"
                    id="outlined-required"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextField
                    name="fullname"
                    label="County"
                    fullWidth
                    margin="dense"
                    id="outlined-required"
                  />
                </Grid>
                <Grid item xs={12} sm={12}>
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">State</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={data}
                      label="Gender"
                      onChange={handleChange}
                    >
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">Country</InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={data}
                      label="Gender"
                      onChange={handleChange}
                    >
                      <MenuItem value={10}>Ten</MenuItem>
                      <MenuItem value={20}>Twenty</MenuItem>
                      <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>

              </Grid>
            </OutlinedDiv>
          </Box>

          <Box
            px={3} py={2}
            container
            direction="row"
          >
            <OutlinedDiv label="Phones">
              <Grid container spacing={1}>

                <Grid item xs={12} sm={12} lg={9}>
                <CustomizedTables/>
                </Grid>

                <Grid item xs={12} sm={12} lg={3} sx={{ marginTop: "2%",padding:"10px" }}>
                  <Button variant="contained" sx={{width:"100%",margin:"0px 0px 15px 10px"}}>Add</Button>
                  <Button variant="outlined" sx={{width:"100%",margin:"0px 0px 15px 10px"}}>Modify</Button>
                  <Button variant="outlined" sx={{width:"100%",margin:"0px 0px 15px 10px"}}>Delete</Button>
                  
                </Grid>
              </Grid>
            </OutlinedDiv>
          </Box>
        </Grid>

       
      </Grid>
      <Grid item xs={12} sm={6} lg={6} sx={{ marginTop: "2%",textAlign:"end" }}   px={3} py={2}>
                  <Button variant="contained">Save</Button> &nbsp;
                  <Button variant="outlined">Cancel</Button>
                </Grid>
                </div>
    </>
  )
}

export default NewPerson