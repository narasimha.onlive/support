import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import './index.css';
// import App from './App';
import Login from './Login';
import Todo from '../src/Todo';
import reportWebVitals from './reportWebVitals';
// import Newperson from './Newperson';
import NewPerson from './NewComponent';
import Form from './form';

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<NewPerson />}/>
        <Route path='/form' element={<Form />} />
        <Route path="/todo" element={<Todo />}/>
      </Routes>
    </BrowserRouter>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <Todo />
//   </React.StrictMode>
// );

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
